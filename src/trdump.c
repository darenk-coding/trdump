/*
 *  Copyright (C) 2017-2018 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __linux__
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#ifdef __linux__
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <limits.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

#include "trdump.h"

int main(int argc, char **argv) {
	unsigned int offset;
	int tr_id;
	int ret = 0;

	if (argc < 2) {
#ifdef __linux__
		printf("%s",
			   "No input file specified.\n"
			   "Usage:\n"
			   "trdump <TR savegame>\n"
			   "trdump --read-tr2-igt <Split Key>\n"
			   "trdump --read-tr2-larainfo\n"
			   "trdump --read-tr3-larainfo\n"
			   "trdump --set-qwop-tr2\n"
			   "trdump --set-pre-qwop-tr2 [X] (X = 1-8 for walk-run-jump positions)\n"
			   "trdump --set-ong-tr2 X (X = 1-8)\n");
#else
		printf("%s",
			   "No input file specified.\n"
			   "Usage:\n"
			   "trdump <TR savegame>\n");
#endif
		return EXIT_FAILURE;
	}

#ifdef __linux__
	if (!strcmp(argv[1], "--read-tr2-igt")) {
		if (argc == 3) {
			return trdump_read_tr2_igt(argv[2]);
		} else {
			return trdump_read_tr2_igt(NULL);
		}
	} else if (!strcmp(argv[1], "--read-tr2-larainfo")) {
		if (argc == 3) {
			return trdump_read_tr2_larainfo(argv[2]);
		} else {
			return trdump_read_tr2_larainfo(NULL);
		}
	} else if (!strcmp(argv[1], "--read-tr3-larainfo")) {
		if (argc == 3) {
			return trdump_read_tr3_larainfo(argv[2]);
		} else {
			return trdump_read_tr3_larainfo(NULL);
		}
	} else if (!strcmp(argv[1], "--set-ong-tr2")) {
		if (argc == 3) {
			return trdump_set_state_tr2(TR2_STATE_ONG + atoi(argv[2]));
		} else {
			printf("--set-ong-tr2 needs a directional number (1-8)\n");
			printf("Example: trdump --set-ong-tr2 3");
			return EXIT_FAILURE;
		}
	} else if (!strcmp(argv[1], "--set-pre-qwop-tr2")) {
		if (argc == 3) {
			return trdump_set_state_tr2(TR2_STATE_PRE_QWOP + atoi(argv[2]));
		} else {
			return trdump_set_state_tr2(TR2_STATE_PRE_QWOP);
		}
	} else if (!strcmp(argv[1], "--set-qwop-tr2")) {
		return trdump_set_state_tr2(TR2_STATE_QWOP);
	}
#endif

	tr_id = trdump_find_tr_savegame_signature(argv[1], &offset);

	if (tr_id) {
		ret = trdump_tr_igt(argv[1], tr_id, offset);
	}

#if (defined(WIN32) || defined(WIN64) || defined(_MSC_VER) || defined(_WIN32))
	printf("Press 'Enter' to continue...");
	fflush(stdout);
	while (getchar() != '\n');
#endif

	if (ret) {
		return EXIT_SUCCESS;
	} else {
		return EXIT_FAILURE;
	}
}

/*==========================================
 * Checks for Tomb Raider 1-3 savegames
 * return values:
 * 0: none found
 * TR1_PC: Tomb Raider 1 PC
 * TR1_PS: Tomb Raider 1 PlayStation
 * TR2_PC: Tomb Raider 2 PC
 * TR2_PS: Tomb Raider 2 PlayStation
 * TR3_PC: Tomb Raider 3 PC
 * TR3_PS: Tomb Raider 3 PlayStation
 *
 * also sets the offset of the block
 *------------------------------------------*/
int trdump_find_tr_savegame_signature(char *filename, unsigned int *offset) {
	FILE *fp;
	size_t filesize;
	char *filebuffer;

	fp = fopen(filename, "rb");

	if (!fp) {
		printf("Can't open file \"%s\"", filename);
		return 0;
	}

	fseek(fp, 0, SEEK_END);
	filesize = ftell(fp);
	rewind(fp);

	if (filesize == SAVE_SIZE_TR1_PC) {
		*offset = 0;
		printf("Seems to be a TR1 PC savegame from filesize\n");
		return TR1_PC;
	} else if (filesize == SAVE_SIZE_TR2_PC) {
		*offset = 0;
		printf("Seems to be a TR2 PC savegame from filesize\n");
		return TR2_PC;
	} else if (filesize == SAVE_SIZE_TR3_PC) {
		*offset = 0;
		printf("Seems to be a TR3 PC savegame from filesize\n");
		return TR3_PC;
	}

	filebuffer = (char*)malloc(filesize + 1);

	if (fread(filebuffer, 1, filesize, fp) != filesize) goto read_err;

	filebuffer[filesize] = '\0';

	for (int i = 0; i < (int)filesize; i++) {
		if (!strcmp(&filebuffer[i], (char*)tr1_ps_signature)) {
			printf("Found TR1 PS1 savegame signature in offset 0x%x\n", i);
			*offset = i;
			return TR1_PS;
		} else if (!strcmp(&filebuffer[i], (char*)tr2_ps_signature)) {
			printf("Found TR2 PS1 savegame signature in offset 0x%x\n", i);
			*offset = i;
			return TR2_PS;
		} else if (!strcmp(&filebuffer[i], (char*)tr3_ps_signature)) {
			printf("Found TR3 PS1 savegame signature in offset 0x%x\n", i);
			*offset = i;
			return TR3_PS;
		}
	}

	free(filebuffer);
	fclose(fp);

	printf("Couldn't find any TR2/3 savegames in file \"%s\"\n", filename);

	return 0;
read_err:
	printf("Read error in file \"%s\"\n", filename);
	free(filebuffer);
	fclose(fp);
	return 0;
}


/*==========================================
 * Parses the ingame times of Tomb Raider 1-3
 * savegames
 *------------------------------------------*/
int trdump_tr_igt(char *filename, int tr_id, unsigned int offset) {
	FILE *fp;
	unsigned char savegame[16384 + 1];

	char time_string[10];
	unsigned int time_ticks;
	unsigned int total_ticks = 0;

	int num_levels;
	unsigned int first_offset, to_next_offset;
	size_t savegame_size;
	const char **levelnames;

	switch (tr_id) {
		case TR1_PC:
			first_offset = 0x199;
			savegame_size = SAVE_SIZE_TR1_PC;
			break;
		case TR1_PS:
			first_offset = 0x334;
			savegame_size = SAVE_SIZE_TR1_PS;
			break;
		case TR2_PC:
			num_levels = 18;
			first_offset = 0x93;
			to_next_offset = 0x28;
			levelnames = tr2_levelnames;
			savegame_size = SAVE_SIZE_TR2_PC;
			break;
		case TR2_PS:
			num_levels = 18;
			first_offset = 0x244;
			to_next_offset = 0x28;
			levelnames = tr2_levelnames;
			savegame_size = SAVE_SIZE_TR2_PS;
			break;
		case TR3_PC:
			num_levels = 19;
			first_offset = 0xf5;
			to_next_offset = 0x2f;
			levelnames = tr3_levelnames;
			savegame_size = SAVE_SIZE_TR3_PC;
			break;
		case TR3_PS:
			num_levels = 19;
			first_offset = 0x2b4;
			to_next_offset = 0x30;
			levelnames = tr3_levelnames;
			savegame_size = SAVE_SIZE_TR3_PS;
			break;
		default:
			printf("Invalid tr_id specified\n");
			return 0;
	}

	fp = fopen(filename, "rb");

	if (!fp) {
		printf("Can't open file %s", filename);
		return 0;
	}

	// Read Savegame into memory
	fseek(fp, offset, SEEK_SET);
	memset(savegame, 0, 16384 + 1);
	fread(savegame, 1, savegame_size, fp);

	fseek(fp, offset, SEEK_SET);

	// TR1 only keeps the time of the current level or the last finished level when
	// saving at the end-of-level save prompt (PS1)
	if (tr_id == TR1_PC || tr_id == TR1_PS) {
		fseek(fp, first_offset, SEEK_CUR);

		if (fread(&time_ticks, 4, 1, fp) != 1) goto read_err;

		trdump_get_time_string_from_ticks(time_ticks, time_string);

		printf("Times for %s:\n%s\n", filename, separator);

		printf("%s - Time of current or finished level\n", time_string);
	} else if (tr_id == TR2_PC || tr_id == TR2_PS) {
		// Point levelstats_tr2_ps pointer to offset
		struct levelstats_tr2 *ls = (void*)&savegame[tr_id&TR2_PC ? OFFSET_LS_TR2_PC : OFFSET_LS_TR2_PS];
		char secrets[5];

		secrets[4] = '\0';

		for (int i = 0; i < num_levels; i++) {
			printf("%s\n%s\n%s\n", separator, tr2_levelnames[i], separator);

			trdump_get_time_string_from_ticks(ls[i].timer, time_string);
			printf("Time Taken: %s (%d frames)\n", time_string, ls[i].timer);

			memset(secrets, 0x20, 4);
			if (ls[i].secrets) {
				if (ls[i].secrets&TR2_SECRET_GOLD)  secrets[0] = 'G';
				if (ls[i].secrets&TR2_SECRET_JADE)  secrets[1] = 'J';
				if (ls[i].secrets&TR2_SECRET_STONE) secrets[2] = 'S';
				printf("Secrets Found: %s\n", secrets);
			} else {
				printf("Secrets Found: None\n");
			}

			printf("Kills: %d\n", ls[i].kills);
			printf("Ammo used: %d\n", ls[i].ammo_used);
			printf("Hits: %d\n", ls[i].hits);
			printf("Health Packs Used: %g\n", ls[i].meds_used*0.5);

			if ((double)ls[i].distance_travelled/445 > 1000.0) {
				printf("Distance Travelled: %.2lfkm\n", (double)ls[i].distance_travelled/(445*1000));
			} else {
				printf("Distance Travelled: %.lfm\n", (double)ls[i].distance_travelled/445);
			}

			printf("%s\n", separator);

			printf("\n%s%s\nItems for the Next Level\n%s%s\n", separator, separator, separator, separator);

			//printf("Health: %d\n", ls[i].health);
			printf("Starting Ammo: ");

			if (ls[i].weapon_flag&TR2_WEAPON_SG) {
				printf("SG: ");
			} else {
				printf("(SG): ");
			}
			printf("%d ", ls[i].sg_ammo/6);

			if (ls[i].weapon_flag&TR2_WEAPON_AP) {
				printf("AP: ");
			} else {
				printf("(AP): ");
			}
			printf("%d ", ls[i].ap_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_UZI) {
				printf("UZI: ");
			} else {
				printf("(UZI): ");
			}
			printf("%d ", ls[i].uzi_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_M16) {
				printf("M16: ");
			} else {
				printf("(M16): ");
			}
			printf("%d ", ls[i].m16_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_GL) {
				printf("GL: ");
			} else {
				printf("(GL): ");
			}
			printf("%d ", ls[i].gl_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_HG) {
				printf("HG: ");
			} else {
				printf("(HG): ");
			}
			printf("%d ", ls[i].hg_ammo);
			printf("\n");

			printf("Small Medipacks: %d ", ls[i].small_med_count);
			printf("Large Medipacks: %d ", ls[i].large_med_count);
			printf("Flares: %d\n", ls[i].flare_count);

			printf("%s%s\n\n", separator, separator);
		}

		// Final statistics
		{
			struct levelstats_tr2 ls_sum;

			memset(&ls_sum, 0, sizeof(struct levelstats_tr2));

			for (int i = 0; i < num_levels; i++) {
				ls_sum.timer += ls[i].timer;

				ls_sum.secrets += ls[i].secrets&TR2_SECRET_GOLD ? 1 : 0;
				ls_sum.secrets += ls[i].secrets&TR2_SECRET_JADE ? 1 : 0;
				ls_sum.secrets += ls[i].secrets&TR2_SECRET_STONE ? 1 : 0;

				ls_sum.kills += ls[i].kills;

				ls_sum.ammo_used += ls[i].ammo_used;

				ls_sum.hits += ls[i].hits;

				ls_sum.meds_used += ls[i].meds_used;

				ls_sum.distance_travelled += ls[i].distance_travelled;
			}

			printf("%s\nFinal Statistics\n%s\n", separator, separator);

			trdump_get_time_string_from_ticks(ls_sum.timer, time_string);
			printf("Time Taken: %s (%d frames)\n", time_string, ls_sum.timer);

			printf("Secrets Found: %d of 48\n", ls_sum.secrets);

			printf("Kills: %d\n", ls_sum.kills);
			printf("Ammo used: %d\n", ls_sum.ammo_used);
			printf("Hits: %d\n", ls_sum.hits);
			printf("Health Packs Used: %g\n", ls_sum.meds_used*0.5);

			if ((double)ls_sum.distance_travelled/445 > 1000.0) {
				printf("Distance Travelled: %.2lfkm\n", (double)ls_sum.distance_travelled/(445*1000));
			} else {
				printf("Distance Travelled: %.lfm\n", (double)ls_sum.distance_travelled/445);
			}

			printf("%s\n\n", separator);
		}

		// Overview of In-Game Times
		printf("Times for %s:\n%s\n", filename, separator);
		for (int i = 0; i < num_levels; i++) {
			trdump_get_time_string_from_ticks(ls[i].timer, time_string);
			printf("%s - %s\n", time_string, levelnames[i]);
			total_ticks += ls[i].timer;
		}

		trdump_get_time_string_from_ticks(total_ticks, time_string);

		printf("%s\n", separator);
		printf("%s - Total In-Game Time\n", time_string);
	} else {
		printf("Times for %s:\n%s\n", filename, separator);

		for (int i = 0; i < num_levels; i++) {
			// Time is measured in ticks equal to 1/30th of a second in an uint32
			if (i < num_levels) {
				if (i == 0) {
					fseek(fp, first_offset, SEEK_CUR);
				} else {
					fseek(fp, to_next_offset, SEEK_CUR);
				}

				if (fread(&time_ticks, 4, 1, fp) != 1) goto read_err;

				trdump_get_time_string_from_ticks(time_ticks, time_string);
			}

			printf("%s - %s\n", time_string, levelnames[i]);
		}

		// Total Time
		trdump_get_time_string_from_ticks(total_ticks, time_string);

		printf("%s\n", separator);
		printf("%s - Total In-Game Time\n", time_string);
	}
	fclose(fp);
	return 1;

read_err:
	printf("Read error in file %s\n", filename);
	fclose(fp);
	return 0;
}

void trdump_get_time_string_from_ticks(uint32_t time_ticks, char *time_string) {
	uint32_t time_hours, time_minutes, time_seconds, time_milliseconds;
	char hours[3], minutes[3], seconds[3], milliseconds[4];

	time_hours = (time_ticks/(30*60*60));
	time_minutes = (time_ticks/(30*60)) % 60;
	time_seconds = (time_ticks/30) % 60;
	time_milliseconds = (time_ticks % 30)*(1000/30.0) + 0.5;

	if (time_hours < 10) {
		sprintf(hours, "0%d", time_hours);
	} else {
		sprintf(hours, "%d", time_hours);
	}

	if (time_minutes < 10) {
		sprintf(minutes, "0%d", time_minutes);
	} else {
		sprintf(minutes, "%d", time_minutes);
	}

	if (time_seconds < 10) {
		sprintf(seconds, "0%d", time_seconds);
	} else {
		sprintf(seconds, "%d", time_seconds);
	}

	if (time_milliseconds == 0) {
		sprintf(milliseconds, "000");
	} else if (time_milliseconds < 100) {
		sprintf(milliseconds, "0%d", time_milliseconds);
	} else {
		sprintf(milliseconds, "%d", time_milliseconds);
	}

	sprintf(time_string, "%s:%s:%s.%s", hours, minutes, seconds, milliseconds);

	return;
}

#ifdef __linux__
int trdump_read_tr2_igt(char *split_key) {
	int pid, mem_fd;
	char maps_file_name[256];
	char mem_file_name[256];

	char time_string[10];
	uint32_t total_time = 0;
	uint32_t total_time_previous = 0;

	uint8_t current_level = 0;

	uint64_t offset_levelstats = 0x00400000 + 0x0011ea2c;
	uint64_t offset_levelstats_current_level = 0x00400000 + 0x0011ee08;

	struct levelstats_tr2 ls[18];
	struct levelstats_tr2 ls_c;

	char *split_command;
	bool level_changed = 0;

	if (split_key) {
		split_command = malloc(strlen("xdotool key ") + strlen(split_key) + strlen(" --delay 500") + 1);

		strcpy(split_command, "xdotool key ");
		strcat(split_command, split_key);
	}

	pid = trdump_get_tr_pid(TR2_ID);

	if (!pid) {
		return 1;
	}
	sprintf(mem_file_name, "/proc/%d/mem", pid);

	printf("%d", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);
	//waitpid(pid, NULL, 0);

	if ((mem_fd = open(mem_file_name, O_RDONLY)) < 0) {
		return 2;
	}

	while (1) {
		lseek(mem_fd, offset_levelstats, SEEK_SET);
		if (read(mem_fd, &ls, 18*sizeof(struct levelstats_tr2)) <= 0) { // Levelstats all levels
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}
		lseek(mem_fd, offset_levelstats_current_level, SEEK_SET);
		if (read(mem_fd, &ls_c, sizeof(struct levelstats_tr2)) <= 0) { // Levelstats current level
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		for (int i = 0; i < 19; i++) {

			if (ls[i].timer == 0) {
				ls[i].timer = ls_c.timer;

				if (current_level != i + 1) { // On level change
					current_level = i + 1;
					level_changed = 1;
				}

				break;
			}
		}

		printf("\n");

		for (int i = 0; i < current_level; i++) {
			total_time_previous = total_time;
			total_time = 0;

			trdump_get_time_string_from_ticks(ls[i].timer, time_string);
			printf("%s - %s\n", time_string, tr2_levelnames[i]);

			if (i == current_level - 1) {
				for (int j = 0; j < 18; j++) {
					total_time += ls[j].timer;
				}

				trdump_get_time_string_from_ticks(total_time, time_string);
				printf("%s - %s\n", time_string, tr2_levelnames[18]);
			}
		}

		if (split_key && (total_time_previous && !total_time || level_changed)) { // New Game, split
			system(split_command);
			level_changed = 0;
		}

		usleep(33333);
	}

	return 4;
}

int trdump_read_tr2_larainfo(char *logfile) {
	int pid, mem_fd;
	char maps_file_name[256];
	char mem_file_name[256];

	struct larainfo_tr2 li, li_old;
	uint64_t offset_larapointer = 0x5207dc;
	uint32_t offset_larainfo;

	FILE *fp_log;

	pid = trdump_get_tr_pid(TR2_ID);

	if (!pid) {
		return 1;
	}
	sprintf(mem_file_name, "/proc/%d/mem", pid);

	printf("%d", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);
	//waitpid(pid, NULL, 0);

	if ((mem_fd = open(mem_file_name, O_RDONLY)) < 0) {
		return 2;
	}

	if (logfile && !(fp_log = fopen(logfile, "w"))) {
		fprintf(stderr, "Cannot write to logfile %s.\n");
		return 5;
	}

	while (1) {
		lseek(mem_fd, offset_larapointer, SEEK_SET);
		if (read(mem_fd, &offset_larainfo, sizeof(int32_t)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		memcpy(&li_old, &li, sizeof(struct larainfo_tr2));

		lseek(mem_fd, offset_larainfo, SEEK_SET);
		if (read(mem_fd, &li, sizeof(struct larainfo_tr2)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}
		//ptrace(PTRACE_DETACH, pid, NULL, NULL);

		printf("X = %.0lf:%d (%d)\n", (double)li.x/1024, li.x % 1024, li.x);
		printf("Y = %.0lf:%d (%d)\n", (double)li.z/1024, li.z % 1024, li.z);
		printf("Z = %.0lf:%d (%d)\n", -(double)li.y/1024, -li.y % 1024, -li.y);
		printf("Angle = %.3lf (%d)\n", (double)li.yaw/0x10000*360, li.yaw);
		printf("Speed horizontal = %d units/frame (real: %.2lf)\n", li.hspeed, sqrt(pow(li_old.x - li.x, 2) + pow(li_old.z - li.z, 2)));
		printf("Speed vertical   = %d units/frame\n", -li.vspeed);
		printf("Health = %.1lf%%\n", (double)li.health/10);
		printf("Room ID = %d\n", li.room);
		printf("Animation Frame = %d\n", li.anim_frame);
		printf("Animation = %d\n", li.animation);
		printf("Air = %d\n", li.air);
		if (-li.vspeed < 0 && !li.air && li.animation < 7) {
			printf("Pre-QWOP = ACTIVE\n", li.air);
		} else {
			printf("Pre-QWOP = inactive\n", li.air);
		}

		//printf("Pitch = %d (+0x%x) Roll: %d (+0x%x)\n", li.pitch, (char*)&li.pitch - (char*)&li, li.roll, (char*)&li.roll - (char*)&li);
		//printf("state = %d (+0x%x)\n", li.air, (char*)&li.air - (char*)&li);

		if (fp_log) {
			fprintf(fp_log, "%d %d %d %d\n", li.x, li.z, -li.y, -li.vspeed);
		}

		usleep(33333);
	}

	return 4;
}

int trdump_read_tr3_larainfo(char *logfile) {
	int pid, mem_fd;
	char maps_file_name[256];
	char mem_file_name[256];

	struct larainfo_tr3 li, li_old;
	uint64_t offset_larapointer = 0x6d62a4;
	uint32_t offset_larainfo;

	FILE *fp_log;

	pid = trdump_get_tr_pid(TR3_ID);

	if (!pid) {
		return 1;
	}
	sprintf(mem_file_name, "/proc/%d/mem", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);
	//waitpid(pid, NULL, 0);

	if ((mem_fd = open(mem_file_name, O_RDONLY)) < 0) {
		return 2;
	}

	if (logfile && !(fp_log = fopen(logfile, "w"))) {
		fprintf(stderr, "Cannot write to logfile %s.\n");
		return 5;
	}

	while (1) {
		lseek(mem_fd, offset_larapointer, SEEK_SET);
		if (read(mem_fd, &offset_larainfo, sizeof(int32_t)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		memcpy(&li_old, &li, sizeof(struct larainfo_tr3));

		lseek(mem_fd, offset_larainfo, SEEK_SET);
		if (read(mem_fd, &li, sizeof(struct larainfo_tr3)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}
		//ptrace(PTRACE_DETACH, pid, NULL, NULL);

		printf("X = %.0lf:%d (%d)\n", (double)li.x/1024, li.x % 1024, li.x);
		printf("Y = %.0lf:%d (%d)\n", (double)li.z/1024, li.z % 1024, li.z);
		printf("Z = %.0lf:%d (%d)\n", -(double)li.y/1024, -li.y % 1024, -li.y);
		printf("Angle = %.3lf (%d)\n", (double)li.yaw/0x10000*360, li.yaw);
		printf("Speed horizontal = %d units/frame\n", li.hspeed);
		printf("Speed horizontal (real) = %.2lf units/frame\n", sqrt(pow(li_old.x - li.x, 2) + pow(li_old.z - li.z, 2)));
		printf("Speed vertical   = %d units/frame\n", -li.vspeed);
		printf("Health = %.1lf%%\n", (double)li.health/10);

		if (fp_log) {
			fprintf(fp_log, "%d %d %d %d\n", li.x, li.z, -li.y, -li.vspeed);
		}

		usleep(33333);
	}

	return 4;
}

int trdump_set_state_tr2(int state) {
	int pid, mem_fd;
	char maps_file_name[256];
	char mem_file_name[256];

	struct larainfo_tr2 li, li_old;
	uint64_t offset_larapointer = 0x5207dc;
	uint32_t offset_larainfo;

	pid = trdump_get_tr_pid(TR2_ID);

	if (!pid) {
		return 1;
	}
	sprintf(mem_file_name, "/proc/%d/mem", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);

	if ((mem_fd = open(mem_file_name, O_RDWR)) < 0) {
		return 2;
	}

	// Read larapointer, then read larainfo
	lseek(mem_fd, offset_larapointer, SEEK_SET);
	if (read(mem_fd, &offset_larainfo, sizeof(int32_t)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}
	memcpy(&li_old, &li, sizeof(struct larainfo_tr2));
	lseek(mem_fd, offset_larainfo, SEEK_SET);
	if (read(mem_fd, &li, sizeof(struct larainfo_tr2)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}

	// Manipulate stuff
	switch (state) {
		case TR2_STATE_PRE_QWOP:
			li.vspeed = 37;
			break;
		case TR2_STATE_PRE_QWOP_POS1:
			li.x = li.x - li.x % 1024 + 468;
			li.z = li.z - li.z % 1024 + 923;
			li.yaw = 13876;
			break;
		case TR2_STATE_PRE_QWOP_POS2:
		case TR2_STATE_PRE_QWOP_POS3:
		case TR2_STATE_PRE_QWOP_POS4:
		case TR2_STATE_PRE_QWOP_POS5:
		case TR2_STATE_PRE_QWOP_POS6:
		case TR2_STATE_PRE_QWOP_POS7:
		case TR2_STATE_PRE_QWOP_POS8:
			printf("Pre-QWOP 2-8 -> TODO\n");
			break;
		case TR2_STATE_QWOP:
			li.air = 8;
			break;
		case TR2_STATE_ONG_1:
			// right side at 270° angle
			li.x = li.x - li.x % 1024 + 923;
			li.z = li.z - li.z % 1024 + 550;
			li.yaw = 56336;
			break;
		case TR2_STATE_ONG_2:
			li.x = li.x - li.x % 1024 + 923;
			li.z = li.z - li.z % 1024 + 500;
			li.yaw = 6*UINT16_MAX/4 - 56336;
			break;
		case TR2_STATE_ONG_3:
			li.x = li.x - li.x % 1024 + 101;
			li.z = li.z - li.z % 1024 + 500;
			li.yaw = 56336 - UINT16_MAX/2;
			break;
		case TR2_STATE_ONG_4:
		case TR2_STATE_ONG_5:
		case TR2_STATE_ONG_6:
		case TR2_STATE_ONG_7:
		case TR2_STATE_ONG_8:
			printf("Old New Glitch state 4-8 -> TODO\n");
			break;
	};

	// Write larainfo back to memory
	lseek(mem_fd, offset_larainfo, SEEK_SET);
	if (write(mem_fd, &li, sizeof(struct larainfo_tr2)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}

	return 4;
}

int trdump_get_tr_pid(int tr_id) {
	char status_file[256];
	char pid_name[256];

	int pid;

	FILE *fp;

	DIR *d;
	struct dirent *dir;

	d = opendir("/proc");
	if (d) {
		while ((dir = readdir(d)) != NULL) {
			if ((pid = atoi(dir->d_name))) {
				sprintf(status_file, "/proc/%d/status", pid);

				if (!(fp = fopen(status_file, "r"))) {
					continue;
				}

				fscanf(fp, "Name:\t%s\n", pid_name);

				if (strcasestr(pid_name, tr_exe_names[tr_id])) {
					fclose(fp);
					return pid;
				}

				fclose(fp);
				//printf("%s\n", pid_name);
				//printf("%s\n", dir->d_name);
			}
		}

		closedir(d);
	}

	return 0;
}
#endif
