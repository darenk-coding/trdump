#
#  trdump
#  Copyright (C) 2016 DarenK
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

PREFIX = /usr/local
INSTDIR = "${PREFIX}/bin"

CURDIR = $(shell pwd)
SRCDIR = $(CURDIR)/src
OBJDIR = $(CURDIR)/obj
BINDIR = $(CURDIR)/bin

_OBJ = trdump.o
OBJ = $(patsubst %,$(OBJDIR)/%,$(_OBJ))

CC = gcc

CFLAGS_std = -std=c11
OPTFLAGS = -O3

CFLAGS = $(CFLAGS_std) $(OPTFLAGS)

LIBS = -lm

$(BINDIR)/trdump: $(OBJ)
	@mkdir -p bin
	@echo -e "LINKER\t$@"
	@$(CC) -o $@ $^ $(LIBS)
	@echo -e "STRIP\t$@"
	@strip $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p obj
	@echo -e "CC\t$@"
	@$(CC) $(CFLAGS) -c $< -o $@

install: $(BINDIR)/trdump
	install -D $< ${INSTDIR}/trdump

uninstall:
	rm -f ${INSTDIR}/trdump

.PHONY: clean

clean:
	rm -rf $(OBJDIR) $(BINDIR)
